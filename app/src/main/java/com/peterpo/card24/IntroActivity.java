package com.peterpo.card24;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class IntroActivity extends AppCompatActivity {
    Button createGame;
    EditText gameValue;
    int sum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        createGame = findViewById(R.id.startButton);
        gameValue = findViewById(R.id.gameValue);
        createGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sum = Integer.parseInt(gameValue.getText().toString());
                } catch (Exception e) {
                    Toast.makeText(IntroActivity.this, "Please input a number", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                intent.putExtra("eval_result", sum);
                startActivity(intent);
            }
        });
    }
}
