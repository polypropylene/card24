package com.peterpo.card24;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.singularsys.jep.EvaluationException;
import com.singularsys.jep.Jep;
import com.singularsys.jep.ParseException;

import java.util.HashSet;
import java.util.Set;


public class MainActivity extends AppCompatActivity {
    Button rePick;
    Button checkInput;
    Button clear;
    Button left;
    Button right;
    Button plus;
    Button minus;
    Button multiply;
    Button divide;
    TextView expression;
    ImageButton[] cards;

    int[] data;
    int[] card;
    int[] imageCount = new int[4];
    int sum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        sum = intent.getIntExtra("eval_result", 24);
        cards = new ImageButton[4];
        cards[0] = findViewById(R.id.card1);
        cards[0].setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View view) {
                clickCard(0);
            }
        });
        cards[1] = findViewById(R.id.card2);
        cards[1].setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View view) {
                clickCard(1);
            }
        });
        cards[2] = findViewById(R.id.card3);
        cards[2].setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View view) {
                clickCard(2);
            }
        });
        cards[3] = findViewById(R.id.card4);
        cards[3].setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View view) {
                clickCard(3);
            }
        });
        rePick = findViewById(R.id.repick);
        rePick.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickCard();
            }
        });
        checkInput = findViewById(R.id.checkinput);
        checkInput.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                for (int j = 0; j < 4; j++) {
                    if (imageCount[j] == 0) {
                        Toast.makeText(MainActivity.this, "Please choose all cards", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                String inputStr = expression.getText().toString();
                if (checkInput(inputStr)) {
                    Toast.makeText(MainActivity.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                    pickCard();
                } else {
                    Toast.makeText(MainActivity.this, "Wrong Answer", Toast.LENGTH_SHORT).show();
                    setClear();
                }
            }
        });

        left = findViewById(R.id.left);
        left.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                expression.append("(");
                flipSymbolsAndCloseBracket(false);
                flipCardsAndOpenBracket(true);
            }
        });
        right = findViewById(R.id.right);
        right.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                expression.append(")");
                flipCardsAndOpenBracket(false);
                flipSymbolsAndCloseBracket(true);
            }
        });
        plus = findViewById(R.id.plus);
        plus.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                expression.append("+");
                flipSymbolsAndCloseBracket(false);
                flipCardsAndOpenBracket(true);
            }
        });
        minus = findViewById(R.id.minus);
        minus.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                expression.append("-");
                flipSymbolsAndCloseBracket(false);
                flipCardsAndOpenBracket(true);
            }
        });
        multiply = findViewById(R.id.multiply);
        multiply.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                expression.append("*");
                flipSymbolsAndCloseBracket(false);
                flipCardsAndOpenBracket(true);
            }
        });
        divide = findViewById(R.id.divide);
        divide.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                expression.append("/");
                flipSymbolsAndCloseBracket(false);
                flipCardsAndOpenBracket(true);
            }
        });
        clear = findViewById(R.id.clear);
        clear.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                setClear();
            }
        });
        expression = findViewById(R.id.input);
        String expText = "Please form an expression such that the result is " + sum;
        expression.setHint(expText);
        initCardImage();
        pickCard();
    }

    private void initCardImage() {
        int resID = getResources().getIdentifier("back_0", "drawable", getPackageName());
        for (int i = 0; i < 4; i++) {
            cards[i].setImageResource(resID);
        }
    }

    private void pickCard() {
        do {
            data = new int[4];
            card = new int[4];
            for (int i = 0; i < 4; i++) {
                int value = randomInRange(1, 13);
                card[i] = randomInRange(0, 3) * 13 + value;
                data[i] = value;
            }
        }
        while (hasRepeated(card));
        setClear();
    }

    public boolean hasRepeated(int[] item) {
        Set<Integer> array = new HashSet<>();
        for (int i : item) {
            if (!array.add(i)) {
                return true;
            }
        }
        return false;
    }

    private void setClear() {
        int resID;
        expression.setText("");
        for (int i = 0; i < 4; i++) {
            imageCount[i] = 0;
            resID = getResources().getIdentifier("card" + card[i], "drawable", getPackageName());
            cards[i].setImageResource(resID);
            cards[i].setClickable(true);
        }
        flipCardsAndOpenBracket(true);
        flipSymbolsAndCloseBracket(false);
    }

    private void clickCard(int i) {
        int resId = getResources().getIdentifier("back_0", "drawable", getPackageName());
        String num;
        int value;
        if (imageCount[i] == 0) {
            cards[i].setImageResource(resId);
            cards[i].setClickable(false);
            value = data[i];
            num = Integer.toString(value);
            expression.append(num);
            imageCount[i]++;
        }
        flipSymbolsAndCloseBracket(true);
        flipCardsAndOpenBracket(false);
    }

    private boolean checkInput(String input) {
        Jep jep = new Jep();
        Object res;
        try {
            jep.parse(input);
            res = jep.evaluate();
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this,
                    "Wrong Expression", Toast.LENGTH_SHORT).show();
            return false;
        } catch (EvaluationException e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this,
                    "Wrong Expression", Toast.LENGTH_SHORT).show();
            return false;
        }
        Double ca = (Double) res;
        return Math.abs(ca - sum) < 1e-6;
    }

    private int randomInRange(int min, int max) {
        return (int) (Math.random() * max + min);
    }

    private void flipCardsAndOpenBracket(boolean flag) {
        for (int i = 0; i < 4; i++) {
            if (imageCount[i] == 0) {
                cards[i].setClickable(flag);
            }
        }
        left.setEnabled(flag);
    }

    private void flipSymbolsAndCloseBracket(boolean flag) {
        plus.setEnabled(flag);
        minus.setEnabled(flag);
        multiply.setEnabled(flag);
        divide.setEnabled(flag);
        right.setEnabled(flag);
    }

}
